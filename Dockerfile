FROM node:latest as build-deps

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN npm -v
RUN node -v

COPY . /usr/src/app

RUN npm install
RUN npm run build

FROM nginx:1.12-alpine
COPY --from=build-deps /usr/src/app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]